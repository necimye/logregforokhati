/**
 * A valid credential must have an valid email and password must be at least 8 characters long,
  at least one digit and an alphabet and also password should not contain white space.

 * @param {string} email Given email
 * @returns {boolean} Whether the given email matches with the regular expression for an email.
 */
const validateEmail = email => {
  const regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  return regex.test(email);
};

/**
 *
 * @param {string} Password Given password
 * @returns {boolean} true/false Whether the password contains at least 8 characters, a digit and an alphabet and non-whitespace.
 */
const validatePassword = Password => {
  // regex for non-whitespace
  const nonWhiteSpace = /^\S*$/;
  // regex for alphabet
  const containsAlphabet = /^(?=.*[a-zA-Z]).*$/;
  // regex for number
  const containsNumber = /^(?=.*[0-9]).*$/;
  // regex for the length between 8 -16
  const hasValidLength = /^.{8,16}$/;

  if (!nonWhiteSpace.test(Password)) {
    return false;
  } else if (!containsAlphabet.test(Password)) {
    return false;
  } else if (!containsNumber.test(Password)) {
    return false;
  } else if (!hasValidLength.test(Password)) {
    return false;
  } else {
    return true;
  }
};

const authValidation = {
  validateEmail,
  validatePassword,
};

export default authValidation;
