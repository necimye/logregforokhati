import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import hospitalService from "./hospitalService";

const initialState = {
  hospitals: [],
  isLoading: false,
  isSuccess: false,
  isError: false,
  message: "",
};

// Add a hospital
export const addHospital = createAsyncThunk(
  "hospitals/add",
  async (hospitalData, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await hospitalService.addHospital(hospitalData, token);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      return thunkAPI.rejectWithValue(message);
    }
  }
);
/**
 * Delete a user goal
 */
export const deleteHospital = createAsyncThunk(
  "hospitals/delete",
  async (hospitalId, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await hospitalService.deleteHospital(hospitalId, token);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      return thunkAPI.rejectWithValue(message);
    }
  }
);

/** 
  Get all hospitals for user for whom hospital belongs.
  Basically if another user has added some hospitals then the new user can't see that hospital as 
  user can see only the hospital which he/she had added. To make this I have protected the routes 
  using JSON Web Token. 
*/
export const getHospitals = createAsyncThunk(
  "hospitals/getAll",
  async (_, thunkAPI) => {
    try {
      const token = thunkAPI.getState().auth.user.token;
      return await hospitalService.getHospitals(token);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();

      return thunkAPI.rejectWithValue(message);
    }
  }
);

export const hospitalSlice = createSlice({
  name: "hosptials",
  initialState,
  reducers: {
    reset: state => initialState,
  },
  extraReducers: builder => {
    builder
      .addCase(addHospital.pending, state => {
        state.isLoading = true;
      })
      .addCase(addHospital.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.hospitals.push(action.payload);
      })
      .addCase(addHospital.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(getHospitals.pending, state => {
        state.isLoading = true;
      })
      .addCase(getHospitals.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.hospitals = action.payload;
      })
      .addCase(getHospitals.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      })
      .addCase(deleteHospital.pending, state => {
        state.isLoading = true;
      })
      .addCase(deleteHospital.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.hospitals = state.hospitals.filter(
          hospital => hospital._id !== action.payload.id
        );
      })
      .addCase(deleteHospital.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.message = action.payload;
      });
  },
});

export const { reset } = hospitalSlice.actions;
export default hospitalSlice.reducer;
