import axios from "axios";

const API_URL = "/api/hospitals/";

// Add a new hospital
const addHospital = async (hospitalData, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await axios.post(API_URL, hospitalData, config);

  return response.data;
};

// Delete a user hospital

const deleteHospital = async (hospitalId, token) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await axios.delete(API_URL + hospitalId, config);
  return response.data;
};

// Get user hospitals for the user who has added the hospital
const getHospitals = async token => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const response = await axios.get(API_URL, config);
  return response.data;
};

const hospitalService = {
  addHospital,
  getHospitals,
  deleteHospital,
};

export default hospitalService;
