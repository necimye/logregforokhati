import moment from 'moment'
import LocalHospitalIcon from '@mui/icons-material/LocalHospital';
import EmailIcon from '@mui/icons-material/Email';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import { blue } from '@mui/material/colors';
import { red } from '@mui/material/colors';
function HospitalItem({ hospital }) {

  return (
    <div className="hospital">
      <div className="info-hospital">
        <h2 className='hospital-name'><LocalHospitalIcon sx={{ color: blue[600], fontSize: 20 }} />{hospital.hospitalName}</h2>
        <h3 className='hospital-locaton'><LocationOnIcon sx={{ fontSize: 13 }} /> {hospital.hospitalLocation}</h3>
        <p className='hospital-email'><EmailIcon sx={{ color: red[900], fontSize: 13 }} />{hospital.hospitalEmail}</p><br></br>
      </div>
      <div className="info-time">
        <p>Member since {moment(hospital.createdAt).fromNow()}</p>
      </div>
    </div>
  )
}

export default HospitalItem
