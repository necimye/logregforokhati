import { useSelector, useDispatch } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import LoginRoundedIcon from '@mui/icons-material/LoginRounded'
import LogoutRoundedIcon from '@mui/icons-material/LogoutRounded'
import { green, red } from '@mui/material/colors';
import { blue } from '@mui/material/colors';
import HowToRegRoundedIcon from '@mui/icons-material/HowToRegRounded';
import LocalHospitalIcon from '@mui/icons-material/LocalHospital';
import { logout, reset } from '../features/auth/authSlice'




function Header() {

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { user } = useSelector((state) => state.auth)

  const onLogout = () => {
    dispatch(logout())
    dispatch(reset())
    toast.success('Logged out successfully')
    navigate('/')
  }
  return (
    <header className='header'>
      <div className="logo">
        <Link to='/'>

          <h1 className='app-title'>Okhati Solutions</h1>
        </Link>
      </div>
      <ul>
        {user ? (
          <>
            <li>
              <Link to='/addHospital'>
                <button className='btn'> <LocalHospitalIcon sx={{ color: blue[400] }} /> Add Hospital</button>
              </Link>
            </li>
            <li>
              <button className='btn' onClick={onLogout}>
                <LogoutRoundedIcon sx={{ color: red[800] }} /> Logout
              </button>
            </li>
          </>

        ) : (<>
          <li>
            <Link to='/login'>
              <LoginRoundedIcon sx={{ color: green[600], fontSize: 30 }} /> Login
            </Link>
          </li>
          <li>
            <Link to='/register'>
              <HowToRegRoundedIcon sx={{ color: green[600], fontSize: 30 }} /> Register
            </Link>
          </li>

        </>)}
      </ul>
    </header >
  )
}

export default Header 