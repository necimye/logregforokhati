import { useState, useEffect } from 'react';
import { addHospital } from '../features/hospitals/hospitalSlice'
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Spinner from '../components/Spinner'
import authValidation from '../features/auth/authValidation'
import { toast } from 'react-toastify';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

function HospitalForm() {


  const [formData, setFormData] = useState({
    hospitalName: '',
    hospitalLocation: '',
    hospitalEmail: '',

  })

  const { hospitalName, hospitalLocation, hospitalEmail } = formData

  const { user } = useSelector((state) => state.auth);

  const { hospitals, isLoading, isError, isSuccess, message } = useSelector((state) => state.hospitals)


  const navigate = useNavigate();
  const dispatch = useDispatch();


  if (!user) {
    navigate('/login');
  }

  useEffect(() => {
    if (isError) {
      toast.error(message);
    }
  }, [isError, isLoading, isSuccess, message, navigate, dispatch, hospitals]);

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))

  }

  const onSubmit = (e) => {
    e.preventDefault()

    if (!authValidation.validateEmail(hospitalEmail)) {
      toast.error('Invalid email!')
    } else {
      dispatch(addHospital(formData))
      toast.success('Added Successfully!')
      navigate('/')
    }
    setFormData({
      hospitalName: '',
      hospitalLocation: '',
      hospitalEmail: '',
    })
  }

  if (isLoading) {
    <Spinner />
  }



  return (
    <>
      <h2>Add Hospital</h2>
      <section className="form">
        <form onSubmit={onSubmit}>
          <Box
            sx={{
              '& .MuiTextField-root': { m: 1, width: '500px' }
            }}>

            <div className="form-group">
              <TextField
                required
                type="text"
                className='form-control'
                id='hospitalName'
                name='hospitalName'
                label='Hospital Name'
                value={hospitalName}
                placeholder='Teku Hospital'
                onChange={onChange}
              >
              </TextField>
              <TextField
                required
                type="text"
                className='form-control'
                id='hospitalLocation'
                name='hospitalLocation'
                label='Hospital Location'
                value={hospitalLocation}
                placeholder='Kathmandu'
                onChange={onChange}
              >
              </TextField>
              <TextField
                required
                type="text"
                className='form-control'
                id='hospitalEmail'
                name='hospitalEmail'
                label='Hospital Email'
                value={hospitalEmail}
                placeholder='teku@gmail.com'
                onChange={onChange}
              >
              </TextField>
            </div>
          </Box>
          <div className="form-group">
            <button type='submit' className='btn btn-block'>
              Submit
            </button>
          </div>
        </form>
      </section>
    </>
  )

}
export default HospitalForm