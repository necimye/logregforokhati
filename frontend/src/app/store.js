import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../features/auth/authSlice";
import hospitalReducer from "../features/hospitals/hospitalSlice";

export const store = configureStore({
  reducer: {
    auth: authReducer,
    hospitals: hospitalReducer,
  },
});
