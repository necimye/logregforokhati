import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import HowToRegRoundedIcon from '@mui/icons-material/HowToRegRounded';
import authValidation from '../features/auth/authValidation';
import { register, reset } from '../features/auth/authSlice'
import Spinner from '../components/Spinner';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

function Register() {

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
    showPassword: false,
  })

  const { name, email, password, confirmPassword } = formData

  const navigate = useNavigate()
  const dispath = useDispatch()

  const { user, isLoading, isError, isSuccess, message } = useSelector((state) => state.auth)

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    if (isSuccess || user) {
      navigate('/')
    }

    dispath(reset())
  }, [user, isError, isSuccess, message, navigate, dispath])


  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }

  const handleClickShowPassword = () => {
    setFormData({
      ...formData,
      showPassword: !formData.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const onSubmit = (e) => {
    e.preventDefault()

    if (!authValidation.validateEmail(email)) {
      toast.error('Enter a valid email')
    } else if (!authValidation.validatePassword(password)) {
      toast.error('Enter a valid password')
    } else if (password !== confirmPassword) {
      toast.error('Password do not match')
    } else {
      const userData = {
        name,
        email,
        password,
      }
      dispath(register(userData))
      toast.success('Registration successful')
    }
  }

  if (isLoading) {
    return <Spinner />
  }

  return <>
    <section className="heading">
      <h1>
        <HowToRegRoundedIcon fontSize='60px' /> Register
      </h1>
      <p>Please create an account</p>
    </section>

    <section className="form">
      <form onSubmit={onSubmit}>
        <Box
          sx={{
            '& .MuiTextField-root': { m: 1, width: '500px' },
          }}
        >
          <div className="form-group">
            <TextField
              required
              type="text"
              className='form-control'
              id='name'
              name='name'
              label='Name'
              value={name}
              placeholder='Enter your name'
              onChange={onChange}

            />
          </div>
          <div className="form-group">
            <TextField
              required
              type="text"
              className='form-control'
              id='email'
              name='email'
              label='Email'
              value={email}
              placeholder='duke@gmail.com'
              onChange={onChange}

            />
          </div>
          <div className="form-group">
            <FormControl sx={{ m: 1, width: '500px' }} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
              <OutlinedInput
                required
                className='form-control'
                id='password'
                name='password'
                label='Password'
                value={password}
                type={formData.showPassword ? 'text' : 'password'}
                placeholder='Enter your password'
                autoComplete='off'
                onChange={onChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {formData.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
            <p className='guide password-guide'>password should be 8 character long must contain one digit and an alphabet</p>
          </div>
          <div className="form-group">
            <FormControl sx={{ m: 1, width: '500px' }} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">Confirm Password</InputLabel>
              <OutlinedInput
                required
                className='form-control'
                id='confirmPassword'
                name='confirmPassword'
                label='Confirm Password'
                value={confirmPassword}
                type={formData.showPassword ? 'text' : 'password'}
                placeholder='Confirm your password'
                autoComplete='off'
                onChange={onChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {formData.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
        </Box>
        <div className="form-group">
          <button type='submit' className='btn btn-block'>
            Submit
          </button>
        </div>
      </form>
    </section>

  </>
}

export default Register