import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { getHospitals, reset } from '../features/hospitals/hospitalSlice'
import Spinner from '../components/Spinner';
import HospitalItem from '../components/HospitalItem';


function Homepage() {

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { user } = useSelector((state) => state.auth);
  const { hospitals, isLoading, isSuccess, isError, message } = useSelector((state) => state.hospitals);



  useEffect(() => {

    if (isError) {
      console.log(message);
    }
    if (!user) {
      navigate('/login')
    }

    dispatch(getHospitals())

    return () => {
      dispatch(reset())
    }
  }, [user, isError, message, dispatch, navigate])


  if (isLoading) {
    return <Spinner />
  }

  return (
    <>
      <h1>Welcome {user && user.name}!</h1>
      <h3 className='make-cursive'>Dear customer, expolre following hospitals to get the information and their services.</h3>
      <section className="content">
        {hospitals.length > 0 ? (
          <div className='hospitals'>
            {hospitals.map((hospital) => (
              <HospitalItem key={hospital._id} hospital={hospital} />
            ))}
          </div>
        ) : (
          <div className="empty-hospital-content">
            <h3> You have not added any hospitals yet. </h3>
            <p> Please add some hospital to give information to public about hospital. </p>
          </div>
        )}

      </section>

    </>
  )
}

export default Homepage