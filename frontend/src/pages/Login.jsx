import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import LoginRoundedIcon from '@mui/icons-material/LoginRounded'
import { login, reset } from '../features/auth/authSlice';
import Spinner from '../components/Spinner';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';

function Login() {

  const [formData, setFormData] = useState({
    email: '',
    password: '',
    showPassword: false,
  })

  const { email, password } = formData

  const navigate = useNavigate()
  const dispath = useDispatch()

  const { user, isLoading, isError, isSuccess, message } = useSelector((state) => state.auth)

  useEffect(() => {
    if (isError) {
      toast.error(message)
    }
    if (isSuccess || user) {

      toast.success('Successfully logged in')
      navigate('/')
    }

    dispath(reset())
  }, [user, isError, isSuccess, message, navigate, dispath])

  const onChange = (e) => {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }))
  }
  const handleClickShowPassword = () => {
    setFormData({
      ...formData,
      showPassword: !formData.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const onSubmit = (e) => {
    e.preventDefault()

    const userData = {
      email,
      password,
    }
    dispath(login(userData))
  }

  if (isLoading) {
    return <Spinner />
  }

  // if (isSuccess) {
  //   toast.success('Successfully logged in')
  // }

  return <>
    <section className="heading">
      <h1>
        <LoginRoundedIcon fontSize='60px' /> Login
      </h1>
      <p>Please login to your account</p>
    </section>

    <section className="form">
      <form onSubmit={onSubmit}>
        <Box
          sx={{
            '& .MuiTextField-root': { m: 1, width: '500px' },
          }}
        >
          <div className="form-group">
            <TextField
              type="text"
              className='form-control'
              id='email'
              name='email'
              label='Email'
              value={email}
              placeholder='duke@gmail.com'
              onChange={onChange}
            />
          </div>
          <div className="form-group">
            <FormControl sx={{ m: 1, width: '500px' }} variant="outlined">
              <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
              <OutlinedInput
                className='form-control'
                id='password'
                name='password'
                label='Password'
                value={password}
                type={formData.showPassword ? 'text' : 'password'}
                placeholder='Enter your password'
                autoComplete='off'
                onChange={onChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      edge="end"
                    >
                      {formData.showPassword ? <VisibilityOff /> : <Visibility />}
                    </IconButton>
                  </InputAdornment>
                }
              />
            </FormControl>
          </div>
        </Box>
        <div className="form-group">
          <button type='submit' className='btn btn-block'>
            Submit
          </button>
        </div>
      </form>
    </section>

  </>
}

export default Login