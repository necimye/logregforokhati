const mongoose = require("mongoose");

const connectDB = async () => {
  const myConnection = await mongoose.connect(process.env.MONGO_URI);

  try {
    console.log(
      `MongoDB connected at : ${myConnection.connection.host}`.blue.underline
    );
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
};

module.exports = connectDB;
