const express = require("express");

const router = express.Router();

const {
  getHospitals,
  addHospital,
  updateHospital,
  deleteHospital,
} = require("../controllers/hospitalController");

const { protect } = require("../middlewares/authMiddleware");

router.route("/").get(protect, getHospitals).post(protect, addHospital);

router
  .route("/:id")
  .put(protect, updateHospital)
  .delete(protect, deleteHospital);

router
  .route("/:id")
  .delete(protect, deleteHospital)
  .put(protect, updateHospital);
module.exports = router;
