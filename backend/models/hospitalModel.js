const mongoose = require("mongoose");

// This User model has the ownership to this model
const hospitalSchema = mongoose.Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: "User",
    },
    hospitalName: {
      type: String,
      required: [true, "Please add a hospital"],
    },
    hospitalLocation: {
      type: String,
      required: [true, "Please enter the hospital location"],
    },
    hospitalEmail: {
      type: String,
      required: [true, "Please enter hospital email"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Hospital", hospitalSchema);
