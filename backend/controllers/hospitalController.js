const asyncHandler = require("express-async-handler");

const Hospital = require("../models/hospitalModel");
const User = require("../models/userModel");

// @desc Get Hospitals
// @route GET api/hospitals
// @access Private
const getHospitals = asyncHandler(async (req, res) => {
  const hospitals = await Hospital.find({ user: req.user.id });
  res.status(200).json(hospitals);
});

// @desc Add Hospitals
// @route POST api/hospitals
// @access Private
const addHospital = asyncHandler(async (req, res) => {
  if (!req.body.hospitalName) {
    res.status(400);
    throw new Error("Please add a hospital field");
  }

  const hospital = await Hospital.create({
    user: req.user.id,
    hospitalName: req.body.hospitalName,
    hospitalLocation: req.body.hospitalLocation,
    hospitalEmail: req.body.hospitalEmail,
  });

  res.status(200).json(hospital);
});

// @desc Update Hospital
// @route PUT api/hospital/:id
// @access Private
const updateHospital = asyncHandler(async (req, res) => {
  const hospital = await Hospital.findById(req.params.id);

  if (!hospital) {
    res.status(400);
    throw new Error("Hospital not found!");
  }

  // checking user
  if (!req.user) {
    res.status(401);
    throw new Error("User not found");
  }

  // Check whether the hospital user and logged in user are same
  if (hospital.user.toString() !== req.user.id) {
    res.status(401);
    throw new Error("User not authorized!");
  }

  const updatedHospital = await Hospital.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      new: true,
    }
  );

  res.status(200).json(updatedHospital);
});

// @desc Delete Hospital
// @route DELETE api/hospitals/:id
// @access Private
const deleteHospital = asyncHandler(async (req, res) => {
  const hospital = Hospital.findById(req.params.id);

  if (!hospital) {
    res.status(400);
    throw new Error("Hospital not found!");
  }

  // checking user
  if (!req.user) {
    res.status(401);
    throw new Error("User not found");
  }

  // Check whether the hospital user and logged in user are same
  if (hospital.user.toString !== req.user.id) {
    res.status(401);
    throw new Error("User is not authorized");
  }

  await hospital.remove();

  res.status(200).json({ id: req.params.id });
});

module.exports = {
  getHospitals,
  addHospital,
  updateHospital,
  deleteHospital,
};
