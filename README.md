## Login and Registration system using React JS and Node JS

### Installation instruction

- Clone this repository
- Then run following commands

```
  cd frontend
```

```
  npm run build
```

- Go back to your root directory

```
  npm start
```

- Now project start to run at port 5000
- Make sure you are in production mode in .env file
- Otherwise you have to do npm run dev to run both backend and frontend
- OR you have to do npm run server in backend directory and npm start in frontend directory

### This is the application deployment link. Kindly requested to check for your own data and watch the project closely
[Login and Registration System made for OKHATI organization by SURAJ POKHREL](https://solutionokhatibyduke.herokuapp.com/login)

## I have also uploaded my CV in this project please watch above closely

# Features of this project
* Password must have at least 8 characters one digit and an alpbabet
* Email must be valid and unique
* Full set of protection applied to all available routes to access APIs
* JSON Web Token used for authorization to add new hospital
* Beautiful alert messages for special events


